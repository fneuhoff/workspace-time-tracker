# Workspace Timer

## Features

* Track how long workspaces are open in minutes precision
    * Interval is configurable
* Show the log in IDE as output log
![Preview of console output](https://gitlab.com/fneuhoff/workspace-time-tracker/-/raw/main/src/docs/console-output.gif)

* Show the log as HTML table in window
![Preview of table](https://gitlab.com/fneuhoff/workspace-time-tracker/-/raw/main/src/docs/show-table.gif)

* Export the log to CSV

## Future tasks
* Add unit tests
* Better handling of workspaces with same name
    * if workspace with same name but different path(s) are opened, ask for merging entries
        * workspace could have been moved
    * place somewhere an advice to use named workspaces

## Release Notes

### 1.0.0 
* First push to marketplace 

### 1.0.1
* Fix wrong publisher set

### 1.0.2
* Remove Yarn as package manager and use NPM instead
* Add documentation of features
* Fix initialization problem
* Update dependencies
    * date-fns
    * eslint
* Switch vsce dependency to @vscode/vsce

### 1.0.3
* Add preview gifs
* Remove useless sanitizer
