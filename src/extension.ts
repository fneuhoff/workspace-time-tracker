// The module 'vscode' contains the VS Code extensibility API
// Import the module and reference it with the alias vscode in your code below

import * as vscode from "vscode";

import { JsonFile } from "./lib/storage/json-file";
import { VSCodeGlobalState } from "./lib/storage/vscode-globalstate";
import { ConsoleOutput } from "./lib/output/console-output";
import { HtmlOutput } from "./lib/output/html-output";
import { CsvExport } from "./lib/export/csv-export";
import { WorkspaceLogger } from "./lib/logger/workspace-logger";
import { WORKSPACE_TIME_TRACKER_KEY } from "./lib/storage";
import { OUTPUT_LOG } from "./lib/editor/output-log";


const ONE_MINUTE_IN_MILLISECONDS = 60000;

export type VERSIONS =
	"1.0.3";


// Runtime values
let intervalId: NodeJS.Timeout;

let logger: WorkspaceLogger | undefined;

const consoleOutput = new ConsoleOutput();
const htmlOutput = new HtmlOutput();
const csvExport = new CsvExport();


/**
 * Callback for delete workspace command
 */
function resetWorkspaceTimerLogCommand(): void {
	if (!logger) {
		return;
	}
	logger.reset();
}


/**
 * Export log as CSV file
 */
async function exportLogAsCSVCommand(): Promise<void> {
	if (!logger) {
		return;
	}
	csvExport.export(await logger.getWorkspaceLog());
}


/**
 * Callback for show timer log command
 */
async function outputWorkspaceTimerLogCommand(): Promise<void> {
	if (!logger) {
		return;
	}
	consoleOutput.show(await logger.getWorkspaceLog());
}


/**
 * Show workspace timer output as HTML
 */
async function showWorkspaceTimerLogCommand(): Promise<void> {
	if (!logger) {
		return;
	}
	htmlOutput.show(await logger.getWorkspaceLog());
}


// this method is called when your extension is activated
// your extension is activated the very first time the command is executed
export async function activate(context: vscode.ExtensionContext): Promise<void> {
	OUTPUT_LOG.logToOutputChannel("Activate: Workspace Timer");

	const workspaceFolders = vscode.workspace.workspaceFolders;
	const workspaceName = vscode.workspace.name;

	// Don't continue if not in a workspace
	if (!workspaceFolders || !workspaceName) {
		return;
	}

	const configuration = vscode
		.workspace
		.getConfiguration(WORKSPACE_TIME_TRACKER_KEY);

	const mode = configuration.get<string>("backupMode") as string;
	const updateIntervalInMinutes = configuration.get<number>("updateIntervalInMinutes") as number;

	const jsonFilePath = configuration.get<string>("dataJsonFilePath");

	if (mode === "file" && jsonFilePath === undefined) {
		vscode.window.showErrorMessage("Workspace Timer: JSON path is missing. Timer is not running!");
		return;
	}

	const storage = mode === "file" ?
		new JsonFile(context, jsonFilePath as string) :
		new VSCodeGlobalState(context);

	logger = new WorkspaceLogger(
		storage
	);

	OUTPUT_LOG.logToOutputChannel(`
			Workspace Timer: 
			Mode: ${mode},
			Backup File: ${jsonFilePath},
			Interval: ${updateIntervalInMinutes},
			Storage: ${storage.name},
			Workspace Folders: ${workspaceFolders.map(f => f.uri).join(", ")},
			Workspace Name: ${workspaceName},
		`);

	context.subscriptions.push(vscode.commands.registerCommand("extension.outputWorkspaceTimerLog", outputWorkspaceTimerLogCommand));
	context.subscriptions.push(vscode.commands.registerCommand("extension.showWorkspaceTimerLog", showWorkspaceTimerLogCommand));
	context.subscriptions.push(vscode.commands.registerCommand("extension.resetWorkspaceTimerLog", resetWorkspaceTimerLogCommand));
	context.subscriptions.push(vscode.commands.registerCommand("extension.exportWorkspaceTimerLog", exportLogAsCSVCommand));

	// Start update interval
	intervalId = setInterval(() => {
		logger?.tick(updateIntervalInMinutes);
	}, updateIntervalInMinutes * ONE_MINUTE_IN_MILLISECONDS);
}


// this method is called when your extension is deactivated
export async function deactivate(): Promise<void> {
	OUTPUT_LOG.logToOutputChannel("Deactivate: Workspace Timer");

	if (intervalId) {
		clearInterval(intervalId);
	}

	if (!logger) {
		return;
	}

	await logger.storeLog();
}
