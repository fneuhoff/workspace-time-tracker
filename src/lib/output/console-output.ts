import { formatDuration, intervalToDuration, sub } from "date-fns";
import { OUTPUT_LOG } from "../editor/output-log";
import { Utils } from "../export/utils";
import type { ILog, ILogEntry } from "../logger/workspace-logger";

export interface IFormattedLog {
  [$day: string]: string[];
}


export class ConsoleOutput {
  show(log: ILog) {
    let formattedLog: string = "";

    Object.entries(log.data).forEach(([dayKey, day]: [string, { [key: string]: ILogEntry; }]) => {
      Object.values(day).forEach((entry) => {
        formattedLog += `${dayKey}: ${entry.workspaceName} - [${Utils.uriListToPathList(entry.workspaceFolders)}] --> ${this.getFormattedDuration(entry.durationInMinutes)} (active: ${this.getFormattedDuration(entry.activeDurationInMinutes)})\n`;
      });
    });

    OUTPUT_LOG.logToOutputChannel("LOG OUTPUT: \n\n");
    OUTPUT_LOG.logToOutputChannel(formattedLog, true);
  }


  /**
 * Get formatted duration
 * The function turns a duration in minutes into a human readable string e.g. 
 * 124 minutes -> 2 hours and 4 minutes
 */
  private getFormattedDuration(durationMinutes: number): string {
    try {
      const date = new Date();
      const earlierDate = sub(date, { minutes: durationMinutes });
      const duration = intervalToDuration({
        start: earlierDate,
        end: date
      });
      return formatDuration(duration, { format: ["hours", "minutes"] });
    } catch {
      return "";
    }
  }
}