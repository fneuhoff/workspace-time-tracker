import { Utils } from "../export/utils";
import { window, ViewColumn } from "vscode";
import type { ILog, ILogEntry } from "../logger/workspace-logger";

export class HtmlOutput {


  show(log: ILog) {
    // Create and show a new webview
    const panel = window.createWebviewPanel(
      "workspaceTimer", // Identifies the type of the webview. Used internally
      "Workspace Timer", // Title of the panel displayed to the user
      ViewColumn.One, // Editor column to show the new webview panel in.
      {} // Webview options. More on these later.
    );

    // And set its HTML content
    panel.webview.html = this.getWebviewContent(log);
  }


  private getWebviewContent(log: ILog): string {

    function wrapTableCell(content: string): string {
      return `<tr>${content}</tr>`;
    }

    const entries: string[] = [];

    Object.entries(log.data).forEach(([dayKey, day]: [string, { [key: string]: ILogEntry; }]) => {
      Object.values(day).forEach((entry) => {
        entries.push(
          wrapTableCell(`
            <td>${dayKey}</td>
            <td>${entry.workspaceName}</td>
            <td>${Utils.uriListToPathList(entry.workspaceFolders)}</td>
            <td>${entry.durationInMinutes ? (entry.durationInMinutes / 60).toFixed(2) : "-"}</td>
            <td>${entry.activeDurationInMinutes ? (entry.activeDurationInMinutes / 60).toFixed(2) : "-"}</td>
          `)
        );
      });
    });

    return `<!DOCTYPE html>
    <html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Cat Coding</title>
    </head>
    <body>
      <style>
        table, th, td {
          border: 1px solid var(--vscode-editor-foreground);
          border-spacing: 0; 
          border-collapse: collapse;
          font-familiy: var(--vscode-editor-font-family);
        }
        td, th {
          border-style: none solid solid none;
          padding: 10px;
        }
      </style>
      <table style="width:100%">
        <tr>
          <th>Day</th>
          <th>Workspace Name</th>
          <th>Workspace Folder</th>
          <th>Absolute Duration</th>
          <th>Active Duration</th>
        </tr>
        ${entries}
      </table>
    </body>
    </html>`;
  }

}