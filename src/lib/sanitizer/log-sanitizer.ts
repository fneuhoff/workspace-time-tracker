import { ILog } from "../logger/workspace-logger";

export class LogSanitizer {

    sanitize(log: ILog): ILog {
        // Currently no sanitization needed
        return log;
    }
}