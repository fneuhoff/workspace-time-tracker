import type { ILog, ILogEntry } from "../logger/workspace-logger";
import { Utils } from "./utils";
import { workspace } from "vscode";

export class CsvExport {

  /**
   * Export log as CSV file
   */
  export(log: ILog): void {

    const header = "Date;Workspace Name;Workspace Path;Duration;Active Duration";
    let text = "";

    Object.entries(log.data).forEach(([dayKey, day]: [string, { [key: string]: ILogEntry; }]) => {
      Object.values(day).forEach((entry) => {
        text += `${dayKey};${entry.workspaceName};${Utils.uriListToPathList(entry.workspaceFolders)};${entry.durationInMinutes};${entry.activeDurationInMinutes}\n`;
      });
    });

    workspace.openTextDocument({ content: `${header}\n${text}`, language: "csv" }).then((document) => {
      document.save();
    });
  }

}