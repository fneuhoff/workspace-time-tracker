import { Uri } from "vscode";


export class Utils {

  static uriListToPathList(uriList: string): string {
    return uriList.split(", ").map(Utils.getPathFromUri).join(", ");
  }


  static getPathFromUri(uri: string): string {
    return Uri.parse(uri).fsPath;
  }

}