import type { ILog } from "../logger/workspace-logger";

export const WORKSPACE_TIME_TRACKER_KEY = "workspace-time-tracker";


export interface IStorage {
  name: string;
  store(log: ILog): void;
  restore(): Promise<ILog>;
}