import type { ExtensionContext } from "vscode";
import { WORKSPACE_TIME_TRACKER_KEY, type IStorage } from ".";
import { LOG_TEMPLATE, type ILog } from "../logger/workspace-logger";


export class VSCodeGlobalState implements IStorage {

  name = "global-state";

  constructor(private readonly context: ExtensionContext) {

  }

  store(log: ILog): void {
    this.context.globalState.update(WORKSPACE_TIME_TRACKER_KEY, log);
  }


  restore(): Promise<ILog> {
    return Promise.resolve(this.context.globalState.get<ILog>(WORKSPACE_TIME_TRACKER_KEY, LOG_TEMPLATE));
  }

}
