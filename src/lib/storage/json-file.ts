import { existsSync, writeFileSync, mkdirSync, readFileSync } from "fs";
import type { IStorage } from ".";
import { dirname, extname } from "path";
import { type ExtensionContext, window } from "vscode";
import { VSCodeGlobalState } from "./vscode-globalstate";
import { LOG_TEMPLATE, type ILog } from "../logger/workspace-logger";

export class JsonFile implements IStorage {

  name = "json-file";

  constructor(
    private readonly context: ExtensionContext,
    private readonly storageFilePath: string
  ) { }


  store(log: ILog): void {
    const dirPath: string = dirname(this.storageFilePath);
    if (!existsSync(dirPath)) {
      mkdirSync(dirPath, { recursive: true });
    }

    if (!existsSync(this.storageFilePath)) {
      window.showWarningMessage(`Workspace Timer: The storage file ${this.storageFilePath} does not exist and will be created!`)
    }

    writeFileSync(
      this.storageFilePath,
      JSON.stringify(log, null, 2)
    );
  }


  restore(): Promise<ILog> {

    if (!existsSync(this.storageFilePath)) {

      if (extname(this.storageFilePath) !== ".json") {
        window.showErrorMessage(`Workspace Timer: The storage file ${this.storageFilePath} is not a JSON file!`);
      }

      return Promise.resolve(LOG_TEMPLATE);
    }

    let log: ILog;

    try {
      log = JSON.parse(readFileSync(this.storageFilePath, 'utf8'));
    } catch {
      log = LOG_TEMPLATE;
    }

    if (this.isEmpty(log)) {
      return this.tryToRestoreFromGlobalState();
    }

    return Promise.resolve(log);
  }


  private async tryToRestoreFromGlobalState(): Promise<ILog> {
    const globalStateStorage = new VSCodeGlobalState(this.context);

    const data = await globalStateStorage.restore();

    if (typeof data === "object" && !this.isEmpty(data)) {
      return window.showWarningMessage("Workspace Timer: JSON file is empty, but data could be restored from VSCode storage. Do you like to restore?", "Restore")
        .then((action): ILog => {
          if (action !== "Restore") {
            return LOG_TEMPLATE;
          }
          // Reset global state store!!
          globalStateStorage.store(LOG_TEMPLATE);
          return data;
        });
    } else {
      return Promise.resolve(LOG_TEMPLATE);
    }
  }


  private isEmpty(obj: {}) {
    return Object.keys(obj).length === 0;
  }

}