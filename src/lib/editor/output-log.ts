import { OutputChannel, window } from "vscode";


export class OutputLog {

  private outputChannel: OutputChannel | undefined;


  /**
   * Log text to the output channel of the extension
   * @param text Text that will be logged to output channel
   */
  logToOutputChannel(text: string, showOutput = false): void {
    if (!this.outputChannel) {
      this.outputChannel = window.createOutputChannel("Workspace timer");
    }
    this.outputChannel.append(`${text}\n`);

    if (showOutput) {
      this.outputChannel.show();
    }
  }

}


export const OUTPUT_LOG = new OutputLog();