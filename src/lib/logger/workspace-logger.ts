import { format, isSameDay } from "date-fns";
import type { IStorage } from "../storage";
import { window, workspace, WorkspaceFolder } from "vscode";
import { OUTPUT_LOG } from "../editor/output-log";
import { VERSIONS } from "../../extension";
import { createHash } from "crypto";
import { LogSanitizer } from "../sanitizer/log-sanitizer";


export interface ILogEntry {
  // Usage duration in minutes
  durationInMinutes: number;
  // Activate duration in minutes
  activeDurationInMinutes: number;
  // Workspace name
  workspaceName: string;
  // Workspace folder path
  workspaceFolders: string;
}

export type DayKey = string & { __brand: "DayKey"; };
export type WorkspaceKey = string & { __brand: "WorkspaceKey"; };

export interface ILog {
  version: VERSIONS;
  data: {
    [key: DayKey]: {
      [key: WorkspaceKey]: ILogEntry;
    };
  }
}

export const LOG_TEMPLATE: ILog = {
  version: "1.0.3",
  data: {}
};


export class WorkspaceLogger {

  private workspaceKey: WorkspaceKey;

  private dayKey!: DayKey;
  private activationDate!: Date;

  private logEntry!: ILogEntry;

  private sanitizer: LogSanitizer = new LogSanitizer();


  constructor(
    private readonly storage: IStorage
  ) {
    this.workspaceKey = WorkspaceLogger.getWorkspaceId(
      WorkspaceLogger.workspaceFoldersToStringArray(workspace.workspaceFolders!)
    );

    this.initValues();
  }


  async tick(minutes: number): Promise<void> {
    // Update focused value
    if (window.state.focused) {
      if (!this.logEntry.activeDurationInMinutes) {
        this.logEntry.activeDurationInMinutes = 0;
      }
      this.logEntry.activeDurationInMinutes += minutes;
    }

    // Update base value
    this.logEntry.durationInMinutes += minutes;

    // In case the lonely dev has nothing better to do than working over night, 
    // this triggers creating a new entry for new day
    if (!isSameDay(this.activationDate, new Date())) {
      await this.storeLog();
      await this.initValues();
    }

    OUTPUT_LOG.logToOutputChannel(`Tick: ${this.logEntry.durationInMinutes}`);
  }


  /**
   * Store workspace timer log
   */
  async storeLog(): Promise<void> {
    const log = await this.getWorkspaceLog();
    this.storage.store(log);
  }


  /**
   * Get workspace log
   */
  async getWorkspaceLog(): Promise<ILog> {
    // Get current stored state
    let log = await this.storage.restore();

    // Run log through compatibility check
    if (log.version !== LOG_TEMPLATE.version) {
      log = this.sanitizer.sanitize(log);
    }

    // Create new log entry if non exists
    if (!log.data) {
      log.data = {};
    }

    if (!log.version) {
      log.version = LOG_TEMPLATE.version;
    }

    if (!log.data[this.dayKey]) {
      log.data[this.dayKey] = {};
    }

    // Update the log entry of current workspace
    log.data[this.dayKey][this.workspaceKey] = this.logEntry;
    
    return log;
  }


  /**
   * Clear workspace log
   */
  reset(): void {
    window.showWarningMessage("Do you really want to delete the Workspace TrackerLog", "Reset")
      .then((action) => {
        if (action !== "Reset") {
          return;
        }
        this.storage.store(LOG_TEMPLATE);
        OUTPUT_LOG.logToOutputChannel("Log resetted!");
      });
  }


  /**
   * Init base values
   */
  private async initValues(): Promise<void> {
    this.activationDate = new Date();

    this.dayKey = format(this.activationDate, "yyyy-MM-dd") as DayKey;

    this.logEntry = await this.getLogEntry();
  }


  /**
   * Get log entry from storage if non exists in storage a new one is created
   */
  private async getLogEntry(): Promise<ILogEntry> {

    const log = await this.storage.restore();

    if (log.data && log.data[this.dayKey] && log.data[this.dayKey][this.workspaceKey]) {
      return log.data[this.dayKey][this.workspaceKey];
    } else {
      return {
        durationInMinutes: 0,
        activeDurationInMinutes: 0,
        workspaceName: workspace.name!,
        workspaceFolders: WorkspaceLogger.workspaceFoldersToStringArray(workspace.workspaceFolders!).join(",")
      };
    }
  }


  static getWorkspaceId(folders: readonly string[]): WorkspaceKey {
    const workspaceUri = folders.join(",");
    const hash = createHash("sha256");
    hash.update(workspaceUri);
    return hash.digest("hex") as WorkspaceKey;
  }


  static workspaceFoldersToStringArray(folders: readonly WorkspaceFolder[]): readonly string[] {
    return folders.map((folder) => folder.uri.toString());
  }

}